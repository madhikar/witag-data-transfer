Thu Aug  5 11:48:41 EDT 2021

This is a skeleton for creating a paper.

submit   - where we store submitted pdfs and reviews
figures  - where diagrams we create go
graphs   - where source and pdf for graphs go
results  - where data used for creating graphs and tables goes

main.tex - this is the paper which includes other tex files.
