
PAPER = main
PDF = $(PAPER).pdf
PDFLATEX = pdflatex

DEPENDS=

SRC = abstract.tex \
  title.tex \
  introduction.tex \
  related-work.tex \
  idea.tex \
  evaluation.tex \
  discussion.tex \
  conclusions.tex \
  acknowledgements.tex \
  references.bib \
  $(PAPER).tex \
  acmart.cls


$(PDF): $(SRC)
	cd graphs; make
	cd figures; make
	$(PDFLATEX) $(PAPER)
	$(PDFLATEX) $(PAPER)
	bibtex $(PAPER)
	$(PDFLATEX) $(PAPER)
	$(PDFLATEX) $(PAPER)

remake:
	-make vclean
	make

clean:
	-/bin/rm -f *.log *.dvi *.aux *.lof *.lot *.toc *.bbl *.blg core 

vclean: clean
	-cd graphs; make vclean
	-cd figures; make vclean
	-/bin/rm -f $(PAPER).ps $(PAPER).pdf

