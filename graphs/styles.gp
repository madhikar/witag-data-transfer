# Some useful colours that are fairly distinguishable
# At least for the fist several colours

set style line 11 lt 1 pt 7 lw 3 lc rgb "red"
set style line 12 lt 1 pt 7 lw 3 lc rgb "web-green"
set style line 13 lt 1 pt 7 lw 3 lc rgb "blue"
set style line 14 lt 1 pt 7 lw 3 lc rgb "dark-violet"
set style line 15 lt 1 pt 7 lw 3 lc rgb "black"
set style line 16 lt 1 pt 7 lw 3 lc rgb "dark-turquoise"
set style line 17 lt 1 pt 7 lw 3 lc rgb "olive"
set style line 18 lt 1 pt 7 lw 3 lc rgb "slategray"
set style line 19 lt 1 pt 7 lw 3 lc rgb "magenta"
set style line 20 lt 1 pt 7 lw 3 lc rgb "orange"
set style line 21 lt 1 pt 7 lw 3 lc rgb "aquamarine"
