
#include <stdio.h>
#include <unistd.h>

struct color {
  char *name; 
  int rgb;
  char r;
  char g;
  char b;
};


struct color green[] = {

  {"white",              0xffffff, 255, 255, 255},
  {"honeydew",           0xf0fff0, 240, 255, 240},
  {"seagreen",           0xc1ffc1, 193, 255, 193},
  {"greenyellow",        0xa0ff20, 160, 255,  32},
  {"chartreuse",         0x7cff40, 124, 255,  64},
  {"spring-green",       0x00ff7f,   0, 255, 127},
  {"green",              0x00ff00,   0, 255,   0},
  {"light-green",        0x90ee90, 144, 238, 144},
  {"web-green",          0x00c000,   0, 192,   0},
  {"sea-green",          0x2e8b57,  46, 139,  87},
  {"forest-green",       0x228b22,  34, 139,  34},
  {"dark-chartreuse",    0x408000,  64, 128,   0},
  {"dark-spring-green",  0x008040,   0, 128,  64},
  {"dark-olivegreen",    0x556b2f,  85, 107,  47},
  {"dark-green",         0x006400,   0, 100,   0},
  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},
};

struct color blue[] = {


  {"white",              0xffffff, 255, 255, 255},
  {"light-cyan",         0xe0ffff, 224, 255, 255},
  {"cyan",               0x00ffff,   0, 255, 255},
  {"dark-cyan",          0x00eeee,   0, 238, 238},
  {"light-turquoise",    0xafeeee, 175, 238, 238},
  {"turquoise",          0x40e0d0,  64, 224, 208},
  {"dark-turquoise",     0x00ced1,   0, 206, 209},
  {"light-blue",         0xadd8e6, 173, 216, 230},
  {"skyblue",            0x87ceeb, 135, 206, 235},
  {"slateblue1",         0x8060ff, 128,  96, 255},
  {"web-blue",           0x0080ff,   0, 128, 255},
  {"royalblue",          0x4169e1,  65, 105, 225},
  {"steelblue",          0x306080,  48,  96, 128},
  {"blue",               0x0000ff,   0,   0, 255},
  {"medium-blue",        0x0000cd,   0,   0, 205},
  {"navy",               0x000080,   0,   0, 128},
  {"dark-blue",          0x00008b,   0,   0, 139},
  {"midnight-blue",      0x191970,  25,  25, 112},

  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},
};

struct color red[] = {

  {"white",              0xffffff, 255, 255, 255},
  {"yellow",             0xffff00,  255, 255,   0},
  {"dark-yellow",        0xc8c800,  200, 200,   0},
  {"yellow4",            0x808000,  128, 128,   0},
  {"coral",              0xff7f50, 255, 127,  80},
  {"light-coral",        0xf08080, 240, 128, 128},
  {"orange",             0xffa500,  255, 165,   0},
  {"sandybrown",         0xffa060, 255, 160,  96},
  {"orange-red",         0xff4500,  255,  69,   0},
  {"red",                0xff0000,  255,   0,   0},
  {"light-red",          0xf03232,  240,  50,  50},
  {"dark-orange",        0xc04000,  192,  64,   0},
  {"brown",              0xa52a2a, 165,  42,  42},
  {"dark-red",           0x8b0000,  139,   0,   0},
  {"orangered4",         0x801400,  128,  20,   0},
  {"brown4",             0x801414, 128,  20,  20},

  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},
};

struct color yellow[] = {


  {"white",              0xffffff, 255, 255, 255},
  {"lemonchiffon",       0xffffc0, 255, 255, 192},
  {"khaki1",             0xffff80, 255, 255, 128},
  {"yellow",             0xffff00, 255, 255,   0},
  {"khaki",              0xf0e68c, 240, 230, 140},
  {"gold",               0xffd700, 255, 215,   0},
  {"orange",             0xffa500, 255, 165,   0},
  {"salmon",             0xfa8072, 250, 128, 114},
  {"dark-salmon",        0xe9967a, 233, 150, 122},
  {"dark-khaki",         0xbdb76b, 189, 183, 107},
  {"light-salmon",       0xffa070, 255, 160, 112},
  {"goldenrod",          0xffc020, 255, 192,  32},
  {"light-goldenrod",    0xeedd82, 238, 221, 130},
  {"dark-goldenrod",     0xb8860b, 184, 134,  11},
  {"dark-yellow",        0xc8c800, 200, 200,   0},
//  {"orange-red",         0xff4500, 255,  69,   0},
  {"yellow4",            0x808000, 128, 128,   0},
  {"dark-orange",        0xc04000, 192,  64,   0},
  {"orangered4",         0x801400, 128,  20,   0},

  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},

};

struct color purple[] = {

  {"white",              0xffffff, 255, 255, 255},
  {"light-pink",         0xffb6c1, 255, 182, 193},
  {"pink",               0xffc0c0, 255, 192, 192},

  {"plum",               0xdda0dd, 221, 160, 221},
  {"orchid",             0xff80ff, 255, 128, 255},
  {"violet",             0xee82ee, 238, 130, 238},
  {"light-magenta",      0xf055f0, 240,  85, 240},
  {"magenta",            0xff00ff, 255,   0, 255},
  {"dark-pink",          0xff1493, 255,  20, 147},
  {"purple",             0xc080ff, 192, 128, 255},
  {"dark-magenta",       0xc000ff, 192,   0, 255},
  {"mediumpurple3",      0x8060c0, 128,  96, 192},
  {"dark-violet",        0x9400d3, 148,   0, 211},
  {"orchid4",            0x804080, 128,  64, 128},
  {"dark-plum",          0x905040, 144,  80,  64},


  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},
};

struct color brown[] = {

  {"white",              0xffffff, 255, 255, 255},
  {"beige",              0xf5f5dc, 245, 245, 220},
  {"antiquewhite",       0xcdc0b0, 205, 192, 176},
  {"bisque",             0xcdb79e, 205, 183, 158},
  {"olive",              0xa08020, 160, 128,  32},
  {"tan1",               0xffa040, 255, 160,  64},
  {"sandybrown",         0xffa060, 255, 160,  96},
  {"sienna1",            0xff8040, 255, 128,  64},
  {"sienna4",            0x804014, 128,  64,  20},
  {"brown",              0xa52a2a, 165,  42,  42},
  {"brown4",             0x801414, 128,  20,  20},

  {"black",              0x000000,   0,   0,   0},
  {0,                        0x0,   0,   0,   0},
};

struct color grey[] = {

/* NOTE: higher numbers are closer to white */
  {"white",              0xffffff, 255, 255, 255},
  {"grey100",            0xffffff, 255, 255, 255},
  {"grey90",             0xe5e5e5, 229, 229, 229},
  {"light-grey",         0xd3d3d3, 211, 211, 211},
  {"grey80",             0xcccccc, 204, 204, 204},
  {"grey",               0xc0c0c0, 192, 192, 192},
  {"grey70",             0xb3b3b3, 179, 179, 179},
  {"slategrey",          0xa0b6cd, 160, 182, 205},
  {"dark-grey",          0xa0a0a0, 160, 160, 160},
  {"grey60",             0x999999, 153, 153, 153},
  {"grey50",             0x7f7f7f, 127, 127, 127},
  {"grey40",             0x666666, 102, 102, 102},
  {"grey30",             0x4d4d4d,  77,  77,  77},
  {"grey20",             0x333333,  51,  51,  51},
  {"grey10",             0x1a1a1a,  26,  26,  26},
  
  {0,                        0x0,   0,   0,   0},
};

struct color *colors[] = {
  &green[0],
  &blue[0],
  &red[0],
  &purple[0],
  &yellow[0],
  &brown[0],
  &grey[0],
};


void
print_top()
{
  printf("set terminal postscript eps color enhanced font \"Times-Roman,24\"\n");
  printf("set style rectangle front fillcolor 'web-green' fillstyle solid border linecolor 'black'\n");
  printf("set xrange[0:55]\n");
  printf("set yrange[0:20]\n");
  printf("set key off\n");
  printf("unset xtics\n");
  printf("unset ytics\n");
  printf("\n");
}

void
print_bottom()
{
   printf("\n");
   printf("plot x linecolor 'white' notitle\n");
}

int
main()
{
   struct color *p = 0;
   int i;
   int objnum = 1;
   int x1 = 0;
   int y1 = 0;
   int x2 = 2;
   int y2 = 1;

   print_top();

   for (i=0; i<(sizeof(colors)/sizeof(struct colors *)); i++) {
     p = colors[i];
     while (p->name != 0) {
       printf("set object %2d rectangle from %2d,%2d  to %2d,%2d ",
           objnum, x1, y1, x2, y2);
       printf("lw 1 fillcolor '%s'\n", p->name);
       printf("set label '%s' at %4.1f,%4.1f font ',12'\n", p->name, (float) x1+2.1, (float) y1 + 0.5);
       y1++;
       y2++;
       p++;
       objnum++;
     }
     printf("\n");
     x1 += 8;
     x2 += 8;
     y1 = 0;
     y2 = 1;
   }
 
   print_bottom();
  // exit(0);
}

